import React, { createContext, useContext, useReducer } from "react";
import ActionMap from "../Models/ActionMap";
import { StoryItem } from "../Models/StoryItem.d";

type GlobalActions = ActionMap<ActionPayload>[keyof ActionMap<ActionPayload>];

type StoryState = {
  /**
   * Array of Chapter ID's
   * TODO once connecting this to an external api, make sure ID's are unique hashes across the internet
   */
  story: StoryItem[];
  history: StoryItem[];
};

const initialState: StoryState = {
  story: [{ id: "", progress: 0, order: 0 }],
  history: [{ id: "", progress: 0, order: 0 }],
};

export enum Types {
  setStory = "SET_STORY",
  setStoryItem = "SET_STORY_ITEM",
  addChapter = "ADD_CHAPTER",
  removeChapter = "REMOVE_CHAPTER",
  toggleChapter = "TOGGLE_CHAPTER",
  setHistory = "SET_HISTORY",
  setHistoryItem = "SET_HISTORY_ITEM",
}

type SetStoryParam = {
  story: StoryItem[];
};
type SetStoryItemParam = {
  id: string;
  item: StoryItem;
};
type AddChapterParam = {
  item: StoryItem;
};
type RemoveChapterParam = {
  id: string;
};
type ToggleChapterParam = {
  id: string;
};
type SetHistoryParam = {
  history: StoryItem[];
};
type SetHistoryItemParam = {
  id: string;
  item: StoryItem;
};

type ActionPayload = {
  [Types.setStory]: SetStoryParam;
  [Types.setStoryItem]: SetStoryItemParam;
  [Types.addChapter]: AddChapterParam;
  [Types.removeChapter]: RemoveChapterParam;
  [Types.toggleChapter]: ToggleChapterParam;
  [Types.setHistory]: SetHistoryParam;
  [Types.setHistoryItem]: SetHistoryItemParam;
};

function setStory(state: StoryState, { story }: SetStoryParam): StoryState {
  state.story = story;
  return { ...state };
}

function setStoryItem(
  state: StoryState,
  { id, item }: SetStoryItemParam
): StoryState {
  state.story = state.story.map((prevItem) => {
    if (prevItem.id === id) return item;
    else return prevItem;
  });
  return { ...state };
}

function addChapter(state: StoryState, { item }: AddChapterParam): StoryState {
  const notFound =
    state.story.find((existingItem) => existingItem.id === item.id) ===
    undefined;
  if (notFound) state.story.push(item);
  return { ...state };
}

function removeChapter(
  state: StoryState,
  { id }: RemoveChapterParam
): StoryState {
  const found = state.story.find((existingItem) => existingItem.id === id) !== undefined;
  if (found)
    state.story = state.story.filter((item) => item.id !== id);
  return { ...state };
}

function toggleChapter(
  state: StoryState,
  { id }: ToggleChapterParam
): StoryState {
  console.log('toggleChapter')
  const notFound =
    state.story.find((item) => item.id === id) === undefined;
  if (notFound) state.story.push({ id: id, progress: 0, order: 0 });
  else state.story = state.story.filter((item) => item.id !== id);
  return { ...state };
}

function setHistory(
  state: StoryState,
  { history }: SetHistoryParam
): StoryState {
  state.history = history;
  return { ...state };
}

function setHistoryItem(
  state: StoryState,
  { id, item }: SetHistoryItemParam
): StoryState {
  state.history = state.history.map((prevItem) => {
    if (prevItem.id === id) return item;
    else return prevItem;
  });
  return { ...state };
}

const reducer = (state: StoryState, action: GlobalActions): StoryState => {
  switch (action.type) {
    case Types.setStory:
      return setStory(state, action.payload);
    case Types.setStoryItem:
      return setStoryItem(state, action.payload);
    case Types.addChapter:
      return addChapter(state, action.payload);
    case Types.removeChapter:
      return removeChapter(state, action.payload);
    case Types.toggleChapter:
      return toggleChapter(state, action.payload);
    case Types.setHistory:
      return setHistory(state, action.payload);
    case Types.setHistoryItem:
      return setHistoryItem(state, action.payload);
    default:
      return state;
  }
};

const StoryContext = createContext<{
  store: StoryState;
  dispatch: React.Dispatch<GlobalActions>;
}>({ store: initialState, dispatch: () => [] });

export const StoryStoreProvider = ({ children }: { children: JSX.Element }) => {
  const [store, dispatch] = useReducer(reducer, initialState);

  return (
    <StoryContext.Provider value={{ store, dispatch }}>
      {children}
    </StoryContext.Provider>
  );
};

export const useStore = () => useContext(StoryContext);

export const useStoryStore = () => {
  const { store, dispatch } = useStore();
  return {
    story: store.story,
    setStory: (story: StoryItem[]) =>
      dispatch({ type: Types.setStory, payload: { story } }),
    addChapter: (item: StoryItem) =>
      dispatch({ type: Types.addChapter, payload: { item } }),
    removeChapter: (id: string) =>
      dispatch({ type: Types.removeChapter, payload: { id } }),
    toggleChapter: (id: string) =>
      dispatch({ type: Types.toggleChapter, payload: { id } }),
    history: store.history,
    setHistory: (history: StoryItem[]) =>
      dispatch({ type: Types.setHistory, payload: { history } }),
  };
};
