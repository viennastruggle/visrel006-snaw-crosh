import React, { createContext, useContext, useReducer } from "react";
import ActionMap from "../Models/ActionMap";
import { Chapter } from "../Models/Chapter";

type GlobalActions = ActionMap<ActionPayload>[keyof ActionMap<ActionPayload>];

type ChapterState = {
  chapter: Chapter;
  isPlaying: boolean;
  volume: number;
  isMute: boolean;
};

const initialState: ChapterState = {
  chapter: {
    id: '',
    title: ''
  },
  isPlaying: false,
  volume: 0.7,
  isMute: false,
};

export enum Types {
  setChapter = "SET_CHAPTER",
  setIsPlaying = "SET_IS_PLAYING",
  setVolume = "SET_VOLUME",
  setIsMute = "SET_IS_MUTE",
}

type SetChapterParam = {
  chapter: Chapter;
};

type SetIsPlayingParam = {
  isPlaying: boolean;
};

type SetVolumeParam = {
  volume: number;
};

type SetIsMuteParam = {
  isMute: boolean;
};

type ActionPayload = {
  [Types.setChapter]: SetChapterParam;
  [Types.setIsPlaying]: SetIsPlayingParam;
  [Types.setVolume]: SetVolumeParam;
  [Types.setIsMute]: SetIsMuteParam;
};

function setChapter(
  state: ChapterState,
  { chapter }: SetChapterParam
): ChapterState {
  state.chapter = chapter;
  return { ...state };
}

function setIsPlaying(
  state: ChapterState,
  { isPlaying }: SetIsPlayingParam
): ChapterState {
  state.isPlaying = isPlaying;
  return { ...state };
}

function setVolume(
  state: ChapterState,
  { volume }: SetVolumeParam
): ChapterState {
  state.volume = volume;
  return { ...state };
}

function setIsMute(
  state: ChapterState,
  { isMute }: SetIsMuteParam
): ChapterState {
  state.isMute = isMute;
  return { ...state };
}

const reducer = (state: ChapterState, action: GlobalActions): ChapterState => {
  switch (action.type) {
    case Types.setChapter:
      return setChapter(state, action.payload);
    case Types.setIsPlaying:
      return setIsPlaying(state, action.payload);
    case Types.setVolume:
      return setVolume(state, action.payload);
    case Types.setIsMute:
      return setIsMute(state, action.payload);
    default:
      return state;
  }
};

const ChapterContext = createContext<{
  store: ChapterState;
  dispatch: React.Dispatch<GlobalActions>;
}>({ store: initialState, dispatch: () => [] });

export const ChapterStoreProvider = ({ children }: { children: JSX.Element }) => {
  const [store, dispatch] = useReducer(reducer, initialState);

  return (
    <ChapterContext.Provider value={{ store, dispatch }}>
      {children}
    </ChapterContext.Provider>
  );
};

export const useStore = () => useContext(ChapterContext);

export const useChapterStore = () => {
  const { store, dispatch } = useStore();
  return {
    chapter: store.chapter,
    setChapter: (chapter: Chapter) =>
      dispatch({ type: Types.setChapter, payload: { chapter } }),
    isPlaying: store.isPlaying,
    setIsPlaying: (isPlaying: boolean) =>
      dispatch({ type: Types.setIsPlaying, payload: { isPlaying } }),
    volume: store.volume,
    isMute: store.isMute,
    setIsMute: (isMute: boolean) =>
      dispatch({ type: Types.setIsMute, payload: { isMute } }),
  };
};
