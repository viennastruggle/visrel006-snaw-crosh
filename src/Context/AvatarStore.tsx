import React, { createContext, useContext, useReducer } from "react";
import ActionMap from "../Models/ActionMap";
import { Avatar } from "../Models/Avatar";

type GlobalActions = ActionMap<ActionPayload>[keyof ActionMap<ActionPayload>];

type AvatarState = {
  avatar: Avatar;
};

const initialState: AvatarState = {
  avatar: {
    id: "unnamed",
    name: "Unnamed",
    storyPlot: [],
    description: `You have not yet defined your avatar. Or choose one from our library.`
  },
};

export enum Types {
  setAvatar = "SET_AVATAR",
}

type SetAvatarParam = {
  avatar: Avatar;
};

type ActionPayload = {
  [Types.setAvatar]: SetAvatarParam;
};

function setAvatar(
  state: AvatarState,
  { avatar }: SetAvatarParam
): AvatarState {
  state.avatar = avatar;
  return { ...state };
}

const reducer = (state: AvatarState, action: GlobalActions): AvatarState => {
  switch (action.type) {
    case Types.setAvatar:
      return setAvatar(state, action.payload);
    default:
      return state;
  }
};

const AvatarContext = createContext<{
  store: AvatarState;
  dispatch: React.Dispatch<GlobalActions>;
}>({ store: initialState, dispatch: () => [] });

export const AvatarStoreProvider = ({ children }: { children: JSX.Element }) => {
  const [store, dispatch] = useReducer(reducer, initialState);

  return (
    <AvatarContext.Provider value={{ store, dispatch }}>
      {children}
    </AvatarContext.Provider>
  );
};

export const useStore = () => useContext(AvatarContext);

export const useAvatarStore = () => {
  const { store, dispatch } = useStore();
  return {
    avatar: store.avatar,
    setAvatar: (avatar: Avatar) =>
      dispatch({ type: Types.setAvatar, payload: { avatar } }),
  };
};
