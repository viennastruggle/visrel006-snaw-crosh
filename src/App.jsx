import React, { useState } from "react";

import styled from "@emotion/styled";
import { Route, Switch, useLocation } from "react-router-dom";
import HomePage from "./Pages/HomePage";
import LobbyPage from "./Pages/LobbyPage";
import LivePage from "./Pages/LivePage";
import SelectAvatarPage from "./Pages/SelectAvatarPage";
import AddChaptersPage from "./Pages/AddChaptersPage";

import { AnimatePresence } from "framer-motion";

import Modal from "./Components/Modal";
import Header from "./Components/Header";

import { AvatarStoreProvider } from "./Context/AvatarStore";
import { ChapterStoreProvider } from "./Context/ChapterStore";
import { StoryStoreProvider } from "./Context/StoryStore";
import ReactHowler from "react-howler";

const Container = styled.div``;

function App() {
  const location = useLocation();
  const [showModal, setShowModal] = useState(false);

  return (
    <Container className="App">
      <script src="https://widget.cloudinary.com/global/all.js"></script>
      <Header />
      <ReactHowler src="/audio/syn3a-sc-10-pad-mix-24bit-2018-02-28.mp3" playing={true} loop={true} volume={0.25} />
     
      <Modal showModal={showModal} setShowModal={setShowModal} />
      <AnimatePresence
        exitBeforeEnter
        onExitComplete={() => setShowModal(false)}
      >
        <AvatarStoreProvider>
          <ChapterStoreProvider>
            <StoryStoreProvider>
              <Switch location={location} key={location.key}>
                <Route path="/avatar">
                  <SelectAvatarPage />
                </Route>
                <Route path="/chapters">
                  <AddChaptersPage />
                </Route>
                <Route path="/lobby">
                  <LobbyPage setShowModal={setShowModal} />
                </Route>
                <Route path="/live">
                  <LivePage />
                </Route>
                <Route path="/">
                  <HomePage />
                </Route>
              </Switch>
            </StoryStoreProvider>
          </ChapterStoreProvider>
        </AvatarStoreProvider>
      </AnimatePresence>
    </Container>
  );
}

export default App;
