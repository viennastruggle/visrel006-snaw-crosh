import { Source } from "../Models/Source";

export type Chapter = {
  id: string;
  title: string;
  teaser?: Source;
  audio?: ChapterAudio;
  video?: ChapterVideo;
};

export type ChapterAudio = {
  source: Source;
  rate?: number;
  volume?: number;
  position?: number;
  isPlaying?: boolean;
};

export type ChapterVideo = {
  source: Source;
  rate?: number;
  volume?: number;
  position?: number;
  isPlaying?: boolean;
};
