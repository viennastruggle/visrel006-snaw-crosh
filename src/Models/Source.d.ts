export type Source = {
  publicId?: string; // cloudinary
  url?: string; // fallback
  mimeType: MimeType;
};

export enum MimeType {
  PNG = "image/png",
  JPG = "image/jpg",
  MPEG = "video/mpeg",
  MOV = "video/quicktime",
  MP4 = "video/mp4",
  WEBM = "video/webm",
  MP3 = "audio/mp3",
}
