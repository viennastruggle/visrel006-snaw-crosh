import { StoryItem } from "./StoryItem.d";
import { Asset } from "./Asset";
import { Source } from "./Source";

export type Avatar = {
  id: string;
  name: string;
  model?: AvatarModel;
  storyPlot: StoryItem[];
  assets?: Asset[];
  theme?: AvatarTheme;
  description?: string;
};

export type AvatarModel = {
  name?: string;
  source: Source;
};

export type AvatarTheme = {
  primaryColor?: string;
  backgroundColor?: string;
  fontFamily?: string;
  logoImage?: Source;
  headerImage?: Source;
};
