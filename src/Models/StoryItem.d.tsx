
export type StoryItem = {
  id: string;
  progress: number;
  order: number; // 0 = unordered, the lower the number the higher the order
};
