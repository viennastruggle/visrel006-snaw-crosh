import { Source } from "./Source";

export type Asset = {
    id: string;
    type: AssetType;
    manifest: AssetManifest;
    name?: string;
    description: string;
    source: Source;
    lastValidated?: string;
}

export enum AssetType {
    REAL_LIFE = "REAL_LIFE",
    HYBRID = "HYBRID",
    VIRTUAL = "VIRTUAL",
    AUDIO = "AUDIO",
    VIDEO = "VIDEO",
    PYRO = "PYRO",
    ACTION = "ACTION",
    COMMAND = "COMMAND",
    PLOTITERATION = "PLOTITERATION",
    AV = "AV",
    WAYPOINT = "WAYPOINT",
    PRODUCT = "PRODUCT",
    DOWNLOAD = "DOWNLOAD",
    TOKEN = "TOKEN"
}

export type AssetManifest = {
    url: string;
    isUnique: boolean;
    tradeable: boolean;
    // TODO find a convincing single source of truth
}
