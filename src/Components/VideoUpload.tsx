import styled from "@emotion/styled";
import { Video } from "cloudinary-react";
import { motion } from "framer-motion";
import React, { useState } from "react";
import { CloudinarySourceEnum } from "../Services/Cloundinary"
import { CLOUDINARY_CLOUD_NAME } from "../config";

type Props = {
  id: string;
  folder: string;
  text?: string;
};

const Container = styled.div`
  width: 100%;
  height: 100%;
  border: 2px dashed #34455e;
  display: flex;
  align-items: center;
  justify-content: center;
  button {
    margin: 1rem;
  }
`;

const buttonVariants = {
  hover: {
    scale: 1.1,
    textShadow: "0px 0px 8px rgb(255,255,255)",
    boxShadow: "0px 0px 8px rgb(255,255,255)",
    transition: {
      duration: 0.3,
      yoyo: Infinity,
    },
  },
};

const VideoUpload = ({ text, id, folder }: Props) => {
  const [isSuccessful, setIsSuccessful] = useState(false);
  const uploadWidget = () => {
    // window.cloudinary.openUploadWidget(
    //   {
    //     publicId: "yt",
    //     folder: folder,
    //     cloudName: CLOUDINARY_CLOUD_NAME,
    //     resourceType: "video",
    //     multiple: false,
    //     showPoweredBy: false,
    //     autoMinimize: true,
    //     upload_preset: "ml_default",
    //     sources: [CloudinarySourceEnum.LOCAL, CloudinarySourceEnum.CAMERA],
    //   },
    //   (error: Error, result: any) => {
    //     console.error(JSON.stringify(error));
    //     if (result) {
    //       console.log(result);
    //       setIsSuccessful(true);
    //     } else {
    //       alert(JSON.stringify(error));
    //     }
    //   }
    // );
  };
  return (
    <Container>
      {!isSuccessful ? (
        <motion.button
          variants={buttonVariants}
          whileHover="hover"
          onClick={uploadWidget}
        >
          {text ? text : "Upload Video"}
        </motion.button>
      ) : (
        <Video publicId={`${folder}/${id}`} width={300} />
      )}
    </Container>
  );
};

export default VideoUpload;
