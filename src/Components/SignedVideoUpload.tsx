import styled from "@emotion/styled";
import { Video } from "cloudinary-react";
import { motion } from "framer-motion";
import React, { useState } from "react";
import { CLOUDINARY_API_KEY, CLOUDINARY_CLOUD_NAME } from "../config";

type Props = {
  id: string;
  folder: string;
  text?: string;
};

const Container = styled.div`
  width: 100%;
  height: 100%;
  border: 2px dashed #34455e;
  display: flex;
  align-items: center;
  justify-content: center;
  button {
    margin: 1rem;
  }
`;

const buttonVariants = {
  hover: {
    scale: 1.1,
    textShadow: "0px 0px 8px rgb(255,255,255)",
    boxShadow: "0px 0px 8px rgb(255,255,255)",
    transition: {
      duration: 0.3,
      yoyo: Infinity,
    },
  },
};

const SignedVideoUpload = ({ text, id, folder }: Props) => {
  const [isSuccessful, setIsSuccessful] = useState(false);

  const uploadWidget = () => {
    // window.cloudinary.openUploadWidget(
    //   {
    //     // publicId: id,
    //     publicId: "yt",
    //     folder: folder,
    //     cloud_name: CLOUDINARY_CLOUD_NAME,
    //     resourceType: "video",
    //     overwrite: true,
    //     multiple: false,
    //     showPoweredBy: false,
    //     autoMinimize: true,
    //     upload_preset: "ml_default",
    //     sources: ["local", "camera"],
    //   },
    //   (error: Error, result: any) => {
    //     console.error(JSON.stringify(error));
    //     if (result) {
    //       console.log(result);
    //       setIsSuccessful(true);
    //     } else {
    //       alert(JSON.stringify(error));
    //     }
    //   }
    // );
  };
  return (
    <Container>
      <div id="cloundinary-widget"></div>
      {!isSuccessful ? (
        <motion.button
          variants={buttonVariants}
          whileHover="hover"
          onClick={uploadWidget}
        >
          {text ? text : "Upload Video"}
        </motion.button>
      ) : (
        <Video publicId={`${folder}/${id}`} width={300} />
      )}
    </Container>
  );
};

export default SignedVideoUpload;
