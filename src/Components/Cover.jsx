import React from "react";

import { Image } from "cloudinary-react";
import styled from "@emotion/styled";

const Container = styled.div`
  img {
    width: 50vw;
    height: 100vh;
    object-fit: cover;
  }
`;

function Cover() {
  return (
    <Container className="Cover">
      <Image publicId="cover" width="1980" crop="scale" />
    </Container>
  );
}

export default Cover;
