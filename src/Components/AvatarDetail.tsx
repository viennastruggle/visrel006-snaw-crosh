import styled from "@emotion/styled";
import { Video } from "cloudinary-react";
import { motion } from "framer-motion";
import React from "react";
import { Avatar } from "../Models/Avatar";
import Inventory from "./Inventory";
import { MimeType } from "../Models/Source.d";
import VideoUpload from "./VideoUpload";

const Container = styled.div``;

type Props = {
  avatar?: Avatar;
};

const containerVariants = {
  hidden: {
    opacity: 0,
    x: "100vw",
  },
  visible: {
    opacity: 1,
    x: 0,
    transition: {
      type: "spring",
      delay: 0.5,
    },
  },
  exit: {
    x: "-100vw",
    transition: { ease: "easeInOut" },
  },
};

const AvatarDetail = ({ avatar }: Props) => {
  return (
    <Container>
      {avatar === undefined ? (
        <>Error loading avatar data</>
      ) : (
        <>
          {avatar.id !== "unnamed" && (
            <motion.div
              variants={containerVariants}
              drag
              dragConstraints={{ left: 0, top: 0, right: 0, bottom: 0 }}
              dragElastic={1}
            >
              <div className="right">
                <motion.div
                  drag
                  dragConstraints={{ left: 0, top: 0, right: 0, bottom: 0 }}
                  dragElastic={1}
                >
                  <h4>{avatar.name}</h4>
                  {avatar.model?.source.mimeType === MimeType.MOV &&
                    avatar.model.source.publicId ? (
                      <Video
                        publicId={avatar.model?.source.publicId}
                        width={300}
                        controls
                      />
                    ):(
                      <div style={{height: "20vh", marginBottom: "1rem"}}>
                        <VideoUpload text="Upload Avatar Video" id={"yt"} folder={'visrel006-snawcrosh/avatars'}/>
                      </div>
                    )}
                </motion.div>
                <motion.div
                  drag
                  dragConstraints={{ left: 0, top: 0, right: 0, bottom: 0 }}
                  dragElastic={1}
                >
                  <p>{avatar.description}</p>
                </motion.div>
                <Inventory assets={avatar.assets} />
              </div>
            </motion.div>
          )}
        </>
      )}
    </Container>
  );
};

export default AvatarDetail;
