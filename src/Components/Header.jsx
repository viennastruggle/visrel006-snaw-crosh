import React from "react";
import { motion } from "framer-motion";
import { meta } from "../Presets/SnawCrosh";
import Loader from "./Loader";
import styled from "@emotion/styled";
// import QR from "./QR";

const Container = styled.header`
  display: flex;
  padding: 1rem;
  align-items: center;
  z-index: 100;
  position: fixed;
  top: 0;
  .title {
    flex-grow: 1;
    margin-left: 20px;
    font-size: 0.6em;
  }
  h1 {
    padding-bottom: 10px;
  }
  .pizza-svg {
    width: 80px;
    overflow: visible;
    stroke: #fff;
    stroke-width: 4;
    stroke-linejoin: round;
    stroke-linecap: round;
  }
  .loader {
    position: fixed; 
    left: 5rem;
    bottom: 0;
  }
`;
const Header = () => {
  return (
    <Container>
      <motion.div
        drag
        dragConstraints={{ left: 0, top: 0, right: 0, bottom: 0 }}
        dragElastic={1}
      >
        {/* <QR/> */}
        {meta.title}
      </motion.div>
      <motion.div
        className="title"
        initial={{ y: -250 }}
        animate={{ y: 0 }}
        transition={{ delay: 2, type: "spring", stiffness: 120 }}
      >
        | Now playing: <b>Pad</b>
      </motion.div>
      <Loader className="loader" text={""} />
    </Container>
  );
};

export default Header;
