import styled from "@emotion/styled";
import { Image } from "cloudinary-react";
import { motion } from "framer-motion";
import React, { useState } from "react";
import useSound from "use-sound";
import { Asset } from "../Models/Asset";

type Props = {
  asset: Asset;
};

const Container = styled.div``;

const nextVariants = {
  hidden: {
    x: "-100vw",
  },
  visible: {
    x: 0,
    transition: { type: "spring", stiffness: 120 },
  },
};
const buttonVariants = {
  hover: {
    scale: 1.1,
    textShadow: "0px 0px 8px rgb(255,255,255)",
    boxShadow: "0px 0px 8px rgb(255,255,255)",
    transition: {
      duration: 0.3,
      yoyo: Infinity,
    },
  },
};

const AssetItem = ({ asset }: Props) => {
  const [isCollapsed, setIsCollapsed] = useState(true);
  const [playActive] = useSound("/audio/pfff.mp3", { volume: 0.25 });
  const [playOn] = useSound("/audio/switch-off.mp3", { volume: 0.25 });
  return (
    <Container>
      <div className="thumbnail">
        <motion.button
          variants={buttonVariants}
          whileHover="hover"
          onClick={() => setIsCollapsed(!isCollapsed)}
          onMouseOver={() => playActive()}
          onMouseUp={() => {
            playOn();
          }}
        >
          <Image publicId={asset.source.publicId} width="60" height="60" />
        </motion.button>
      </div>
      {!isCollapsed && (
        <motion.div className="details" variants={nextVariants}>
          <h4>
            <span className="name">{asset.name}</span>
          </h4>
          <span className="type">{asset.type}</span>
          <p className="description">{asset.description}</p>
        </motion.div>
      )}
    </Container>
  );
};

export default AssetItem;
