import styled from "@emotion/styled";
import React from "react";
import { Asset } from "../Models/Asset";
import AssetItem from "./AssetItem";

type Props = {
  assets?: Asset[];
};

const Container = styled.section`
  .items {
    display: grid;
    grid-template-columns: repeat(8, 1fr);
  }
`;

const Inventory = ({ assets }: Props) => {
  return (
    <Container>
      <h5>Inventory</h5>
      <div className="items">
        {assets?.map((item) => (
          <AssetItem key={item.id} asset={item} />
        ))}
      </div>
    </Container>
  );
};

export default Inventory;
