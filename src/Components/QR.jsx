import React from "react";

const QR = () => {
  return (
    <svg
      x="0px"
      y="0px"
      width="231px"
      height="231px"
      viewBox="0 0 231 231"
      enable-background="new 0 0 231 231"
    >
      <rect x="0" y="0" width="231" height="231" fill="rgb(255,255,255)" />
      <g transform="translate(14,14)">
        <g>
          <defs>
            <linearGradient gradientTransform="rotate(90)" id="grad">
              <stop offset="5%" stop-color="rgb(0,0,0)" />
              <stop offset="95%" stop-color="rgb(143,37,5)" />
            </linearGradient>
            <mask id="gmask">
              <g>
                <g transform="translate(56,0) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,0v14c0,0-9.151-4.381-14-0.551C-0.001,13.449-0.242,0,13.999,0z" />
                  </g>
                </g>
                <g transform="translate(63,0) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(70,0) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,13.999H0V0.028v-0.029h3.43C9.269-0.001,13.999,6.267,13.999,13.999z" />
                  </g>
                </g>
                <g transform="translate(98,0) scale(0.014,0.014)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M250,0c138.077,0,250,111.93,250,250c0,138.077-111.923,250-250,250C111.93,500,0,388.077,0,250C0,111.93,111.93,0,250,0z" />
                  </g>
                </g>
                <g transform="translate(126,0) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(70,7) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M-0.001,0l14,0c0,0-4.381,9.151-0.551,14C13.448,14-0.001,14.24-0.001,0z" />
                  </g>
                </g>
                <g transform="translate(119,7) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(126,7) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(133,7) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,13.999H0V0.028v-0.029h3.43C9.269-0.001,13.999,6.267,13.999,13.999z" />
                  </g>
                </g>
                <g transform="translate(63,14) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(77,14) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(91,14) scale(0.014,0.014)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M250,0c138.077,0,250,111.93,250,250c0,138.077-111.923,250-250,250C111.93,500,0,388.077,0,250C0,111.93,111.93,0,250,0z" />
                  </g>
                </g>
                <g transform="translate(105,14) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,0v14c0,0-9.151-4.381-14-0.551C-0.001,13.449-0.242,0,13.999,0z" />
                  </g>
                </g>
                <g transform="translate(112,14) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(119,14) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(126,14) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(133,14) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(140,14) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,13.999H0V0.028v-0.029h3.43C9.269-0.001,13.999,6.267,13.999,13.999z" />
                  </g>
                </g>
                <g transform="translate(56,21) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(63,21) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(70,21) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(77,21) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(84,21) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,13.999H0V0.028v-0.029h3.43C9.269-0.001,13.999,6.267,13.999,13.999z" />
                  </g>
                </g>
                <g transform="translate(98,21) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(112,21) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(126,21) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(133,21) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(140,21) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(56,28) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(63,28) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(84,28) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0-0.001h14v13.97v0.029l-3.431,0.001C4.731,13.999,0,7.731,0-0.001z" />
                  </g>
                </g>
                <g transform="translate(91,28) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(98,28) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(105,28) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(112,28) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(119,28) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(126,28) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(140,28) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M-0.001,0l14,0c0,0-4.381,9.151-0.551,14C13.448,14-0.001,14.24-0.001,0z" />
                  </g>
                </g>
                <g transform="translate(56,35) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(77,35) scale(0.014,0.014)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M250,0c138.077,0,250,111.93,250,250c0,138.077-111.923,250-250,250C111.93,500,0,388.077,0,250C0,111.93,111.93,0,250,0z" />
                  </g>
                </g>
                <g transform="translate(91,35) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0-0.001h14v13.97v0.029l-3.431,0.001C4.731,13.999,0,7.731,0-0.001z" />
                  </g>
                </g>
                <g transform="translate(98,35) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(112,35) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(119,35) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(133,35) scale(0.014,0.014)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M250,0c138.077,0,250,111.93,250,250c0,138.077-111.923,250-250,250C111.93,500,0,388.077,0,250C0,111.93,111.93,0,250,0z" />
                  </g>
                </g>
                <g transform="translate(56,42) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(70,42) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(84,42) scale(0.014,0.014)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M250,0c138.077,0,250,111.93,250,250c0,138.077-111.923,250-250,250C111.93,500,0,388.077,0,250C0,111.93,111.93,0,250,0z" />
                  </g>
                </g>
                <g transform="translate(98,42) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(112,42) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(126,42) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(140,42) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(56,49) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(63,49) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(70,49) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(77,49) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0c0,0,9.15,4.38,14,0.55C14,0.55,14.24,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(98,49) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(112,49) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(119,49) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(126,49) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(140,49) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(0,56) scale(0.014,0.014)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M250,0c138.077,0,250,111.93,250,250c0,138.077-111.923,250-250,250C111.93,500,0,388.077,0,250C0,111.93,111.93,0,250,0z" />
                  </g>
                </g>
                <g transform="translate(28,56) scale(0.014,0.014)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M250,0c138.077,0,250,111.93,250,250c0,138.077-111.923,250-250,250C111.93,500,0,388.077,0,250C0,111.93,111.93,0,250,0z" />
                  </g>
                </g>
                <g transform="translate(42,56) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,0v14c0,0-9.151-4.381-14-0.551C-0.001,13.449-0.242,0,13.999,0z" />
                  </g>
                </g>
                <g transform="translate(49,56) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(56,56) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(63,56) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(84,56) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,0v14c0,0-9.151-4.381-14-0.551C-0.001,13.449-0.242,0,13.999,0z" />
                  </g>
                </g>
                <g transform="translate(91,56) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(98,56) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(112,56) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(119,56) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(140,56) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0-0.001h14v13.97v0.029l-3.431,0.001C4.731,13.999,0,7.731,0-0.001z" />
                  </g>
                </g>
                <g transform="translate(147,56) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(154,56) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(161,56) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(168,56) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(175,56) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,13.999H0V0.028v-0.029h3.43C9.269-0.001,13.999,6.267,13.999,13.999z" />
                  </g>
                </g>
                <g transform="translate(196,56) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(7,63) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(14,63) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(21,63) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,13.999H0V0.028v-0.029h3.43C9.269-0.001,13.999,6.267,13.999,13.999z" />
                  </g>
                </g>
                <g transform="translate(35,63) scale(0.014,0.014)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M250,0c138.077,0,250,111.93,250,250c0,138.077-111.923,250-250,250C111.93,500,0,388.077,0,250C0,111.93,111.93,0,250,0z" />
                  </g>
                </g>
                <g transform="translate(56,63) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(63,63) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(70,63) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0c0,0,9.15,4.38,14,0.55C14,0.55,14.24,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(105,63) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(112,63) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(126,63) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(147,63) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(154,63) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(161,63) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(168,63) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(175,63) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(182,63) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(189,63) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(196,63) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(7,70) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(21,70) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(28,70) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,13.999H0V0.028v-0.029h3.43C9.269-0.001,13.999,6.267,13.999,13.999z" />
                  </g>
                </g>
                <g transform="translate(42,70) scale(0.014,0.014)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M250,0c138.077,0,250,111.93,250,250c0,138.077-111.923,250-250,250C111.93,500,0,388.077,0,250C0,111.93,111.93,0,250,0z" />
                  </g>
                </g>
                <g transform="translate(56,70) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(63,70) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(91,70) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(98,70) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(105,70) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(119,70) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(126,70) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(140,70) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(147,70) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(154,70) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(161,70) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(168,70) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(196,70) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(0,77) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,0v14c0,0-9.151-4.381-14-0.551C-0.001,13.449-0.242,0,13.999,0z" />
                  </g>
                </g>
                <g transform="translate(7,77) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(21,77) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0-0.001h14v13.97v0.029l-3.431,0.001C4.731,13.999,0,7.731,0-0.001z" />
                  </g>
                </g>
                <g transform="translate(28,77) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(49,77) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(56,77) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(63,77) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(84,77) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(91,77) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(119,77) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(133,77) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,0v14c0,0-9.151-4.381-14-0.551C-0.001,13.449-0.242,0,13.999,0z" />
                  </g>
                </g>
                <g transform="translate(140,77) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(168,77) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0-0.001h14v13.97v0.029l-3.431,0.001C4.731,13.999,0,7.731,0-0.001z" />
                  </g>
                </g>
                <g transform="translate(175,77) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0c0,0,9.15,4.38,14,0.55C14,0.55,14.24,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(189,77) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(196,77) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(14,84) scale(0.014,0.014)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M250,0c138.077,0,250,111.93,250,250c0,138.077-111.923,250-250,250C111.93,500,0,388.077,0,250C0,111.93,111.93,0,250,0z" />
                  </g>
                </g>
                <g transform="translate(35,84) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,0v14c0,0-9.151-4.381-14-0.551C-0.001,13.449-0.242,0,13.999,0z" />
                  </g>
                </g>
                <g transform="translate(42,84) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(49,84) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(56,84) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(63,84) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(70,84) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0c0,0,9.15,4.38,14,0.55C14,0.55,14.24,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(84,84) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(91,84) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(105,84) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(119,84) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M-0.001,0l14,0c0,0-4.381,9.151-0.551,14C13.448,14-0.001,14.24-0.001,0z" />
                  </g>
                </g>
                <g transform="translate(140,84) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0-0.001h14v13.97v0.029l-3.431,0.001C4.731,13.999,0,7.731,0-0.001z" />
                  </g>
                </g>
                <g transform="translate(147,84) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,13.999H0V0.028v-0.029h3.43C9.269-0.001,13.999,6.267,13.999,13.999z" />
                  </g>
                </g>
                <g transform="translate(189,84) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(7,91) scale(0.014,0.014)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M250,0c138.077,0,250,111.93,250,250c0,138.077-111.923,250-250,250C111.93,500,0,388.077,0,250C0,111.93,111.93,0,250,0z" />
                  </g>
                </g>
                <g transform="translate(56,91) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M-0.001,0l14,0c0,0-4.381,9.151-0.551,14C13.448,14-0.001,14.24-0.001,0z" />
                  </g>
                </g>
                <g transform="translate(84,91) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0-0.001h14v13.97v0.029l-3.431,0.001C4.731,13.999,0,7.731,0-0.001z" />
                  </g>
                </g>
                <g transform="translate(91,91) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(98,91) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(105,91) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(112,91) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,13.999H0V0.028v-0.029h3.43C9.269-0.001,13.999,6.267,13.999,13.999z" />
                  </g>
                </g>
                <g transform="translate(126,91) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(147,91) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(154,91) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0c0,0,9.15,4.38,14,0.55C14,0.55,14.24,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(168,91) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(175,91) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(182,91) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(189,91) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(196,91) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,13.999H0V0.028v-0.029h3.43C9.269-0.001,13.999,6.267,13.999,13.999z" />
                  </g>
                </g>
                <g transform="translate(0,98) scale(0.014,0.014)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M250,0c138.077,0,250,111.93,250,250c0,138.077-111.923,250-250,250C111.93,500,0,388.077,0,250C0,111.93,111.93,0,250,0z" />
                  </g>
                </g>
                <g transform="translate(14,98) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(21,98) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0c0,0,9.15,4.38,14,0.55C14,0.55,14.24,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(42,98) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,0v14c0,0-9.151-4.381-14-0.551C-0.001,13.449-0.242,0,13.999,0z" />
                  </g>
                </g>
                <g transform="translate(49,98) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,13.999H0V0.028v-0.029h3.43C9.269-0.001,13.999,6.267,13.999,13.999z" />
                  </g>
                </g>
                <g transform="translate(63,98) scale(0.014,0.014)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M250,0c138.077,0,250,111.93,250,250c0,138.077-111.923,250-250,250C111.93,500,0,388.077,0,250C0,111.93,111.93,0,250,0z" />
                  </g>
                </g>
                <g transform="translate(105,98) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0-0.001h14v13.97v0.029l-3.431,0.001C4.731,13.999,0,7.731,0-0.001z" />
                  </g>
                </g>
                <g transform="translate(112,98) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(119,98) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(126,98) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(140,98) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,0v14c0,0-9.151-4.381-14-0.551C-0.001,13.449-0.242,0,13.999,0z" />
                  </g>
                </g>
                <g transform="translate(147,98) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(168,98) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0-0.001h14v13.97v0.029l-3.431,0.001C4.731,13.999,0,7.731,0-0.001z" />
                  </g>
                </g>
                <g transform="translate(175,98) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(182,98) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(196,98) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(7,105) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(14,105) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(28,105) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(49,105) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(84,105) scale(0.014,0.014)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M250,0c138.077,0,250,111.93,250,250c0,138.077-111.923,250-250,250C111.93,500,0,388.077,0,250C0,111.93,111.93,0,250,0z" />
                  </g>
                </g>
                <g transform="translate(98,105) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(112,105) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(119,105) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(126,105) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(133,105) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,13.999H0V0.028v-0.029h3.43C9.269-0.001,13.999,6.267,13.999,13.999z" />
                  </g>
                </g>
                <g transform="translate(154,105) scale(0.014,0.014)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M250,0c138.077,0,250,111.93,250,250c0,138.077-111.923,250-250,250C111.93,500,0,388.077,0,250C0,111.93,111.93,0,250,0z" />
                  </g>
                </g>
                <g transform="translate(189,105) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(196,105) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(0,112) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(7,112) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(14,112) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(28,112) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(42,112) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,0v14c0,0-9.151-4.381-14-0.551C-0.001,13.449-0.242,0,13.999,0z" />
                  </g>
                </g>
                <g transform="translate(49,112) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(56,112) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(63,112) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0c0,0,9.15,4.38,14,0.55C14,0.55,14.24,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(77,112) scale(0.014,0.014)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M250,0c138.077,0,250,111.93,250,250c0,138.077-111.923,250-250,250C111.93,500,0,388.077,0,250C0,111.93,111.93,0,250,0z" />
                  </g>
                </g>
                <g transform="translate(91,112) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,0v14c0,0-9.151-4.381-14-0.551C-0.001,13.449-0.242,0,13.999,0z" />
                  </g>
                </g>
                <g transform="translate(98,112) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(112,112) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(119,112) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(126,112) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(133,112) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(140,112) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(147,112) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0c0,0,9.15,4.38,14,0.55C14,0.55,14.24,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(161,112) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(189,112) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(0,119) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0-0.001h14v13.97v0.029l-3.431,0.001C4.731,13.999,0,7.731,0-0.001z" />
                  </g>
                </g>
                <g transform="translate(7,119) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(28,119) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M-0.001,0l14,0c0,0-4.381,9.151-0.551,14C13.448,14-0.001,14.24-0.001,0z" />
                  </g>
                </g>
                <g transform="translate(49,119) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(70,119) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(105,119) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(112,119) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(119,119) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(154,119) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,0v14c0,0-9.151-4.381-14-0.551C-0.001,13.449-0.242,0,13.999,0z" />
                  </g>
                </g>
                <g transform="translate(161,119) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(168,119) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(175,119) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0c0,0,9.15,4.38,14,0.55C14,0.55,14.24,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(189,119) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0-0.001h14v13.97v0.029l-3.431,0.001C4.731,13.999,0,7.731,0-0.001z" />
                  </g>
                </g>
                <g transform="translate(196,119) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,13.999H0V0.028v-0.029h3.43C9.269-0.001,13.999,6.267,13.999,13.999z" />
                  </g>
                </g>
                <g transform="translate(21,126) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(42,126) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,0v14c0,0-9.151-4.381-14-0.551C-0.001,13.449-0.242,0,13.999,0z" />
                  </g>
                </g>
                <g transform="translate(49,126) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(63,126) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(70,126) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(77,126) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(84,126) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(91,126) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(98,126) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(105,126) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(119,126) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(126,126) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,13.999H0V0.028v-0.029h3.43C9.269-0.001,13.999,6.267,13.999,13.999z" />
                  </g>
                </g>
                <g transform="translate(168,126) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M-0.001,0l14,0c0,0-4.381,9.151-0.551,14C13.448,14-0.001,14.24-0.001,0z" />
                  </g>
                </g>
                <g transform="translate(182,126) scale(0.014,0.014)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M250,0c138.077,0,250,111.93,250,250c0,138.077-111.923,250-250,250C111.93,500,0,388.077,0,250C0,111.93,111.93,0,250,0z" />
                  </g>
                </g>
                <g transform="translate(196,126) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(21,133) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(35,133) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(56,133) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(63,133) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(70,133) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(91,133) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(112,133) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,0v14c0,0-9.151-4.381-14-0.551C-0.001,13.449-0.242,0,13.999,0z" />
                  </g>
                </g>
                <g transform="translate(119,133) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(126,133) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(133,133) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(140,133) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(147,133) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,13.999H0V0.028v-0.029h3.43C9.269-0.001,13.999,6.267,13.999,13.999z" />
                  </g>
                </g>
                <g transform="translate(189,133) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,0v14c0,0-9.151-4.381-14-0.551C-0.001,13.449-0.242,0,13.999,0z" />
                  </g>
                </g>
                <g transform="translate(196,133) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(0,140) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,0v14c0,0-9.151-4.381-14-0.551C-0.001,13.449-0.242,0,13.999,0z" />
                  </g>
                </g>
                <g transform="translate(7,140) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(14,140) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(21,140) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(28,140) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(35,140) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(42,140) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0c0,0,9.15,4.38,14,0.55C14,0.55,14.24,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(56,140) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(70,140) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(91,140) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(119,140) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M-0.001,0l14,0c0,0-4.381,9.151-0.551,14C13.448,14-0.001,14.24-0.001,0z" />
                  </g>
                </g>
                <g transform="translate(140,140) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(147,140) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(154,140) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(161,140) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(168,140) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(175,140) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0c0,0,9.15,4.38,14,0.55C14,0.55,14.24,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(196,140) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(56,147) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(70,147) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(77,147) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0c0,0,9.15,4.38,14,0.55C14,0.55,14.24,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(91,147) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0-0.001h14v13.97v0.029l-3.431,0.001C4.731,13.999,0,7.731,0-0.001z" />
                  </g>
                </g>
                <g transform="translate(98,147) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(105,147) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(112,147) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0c0,0,9.15,4.38,14,0.55C14,0.55,14.24,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(133,147) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(140,147) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(168,147) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(196,147) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(56,154) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M-0.001,0l14,0c0,0-4.381,9.151-0.551,14C13.448,14-0.001,14.24-0.001,0z" />
                  </g>
                </g>
                <g transform="translate(70,154) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M-0.001,0l14,0c0,0-4.381,9.151-0.551,14C13.448,14-0.001,14.24-0.001,0z" />
                  </g>
                </g>
                <g transform="translate(105,154) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M-0.001,0l14,0c0,0-4.381,9.151-0.551,14C13.448,14-0.001,14.24-0.001,0z" />
                  </g>
                </g>
                <g transform="translate(133,154) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0-0.001h14v13.97v0.029l-3.431,0.001C4.731,13.999,0,7.731,0-0.001z" />
                  </g>
                </g>
                <g transform="translate(140,154) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(154,154) scale(0.014,0.014)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M250,0c138.077,0,250,111.93,250,250c0,138.077-111.923,250-250,250C111.93,500,0,388.077,0,250C0,111.93,111.93,0,250,0z" />
                  </g>
                </g>
                <g transform="translate(168,154) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(175,154) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(182,154) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0c0,0,9.15,4.38,14,0.55C14,0.55,14.24,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(196,154) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(63,161) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(77,161) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,0v14c0,0-9.151-4.381-14-0.551C-0.001,13.449-0.242,0,13.999,0z" />
                  </g>
                </g>
                <g transform="translate(84,161) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,13.999H0V0.028v-0.029h3.43C9.269-0.001,13.999,6.267,13.999,13.999z" />
                  </g>
                </g>
                <g transform="translate(98,161) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(119,161) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(126,161) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0c0,0,9.15,4.38,14,0.55C14,0.55,14.24,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(140,161) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(168,161) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(189,161) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(196,161) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(56,168) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,0v14c0,0-9.151-4.381-14-0.551C-0.001,13.449-0.242,0,13.999,0z" />
                  </g>
                </g>
                <g transform="translate(63,168) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(70,168) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0c0,0,9.15,4.38,14,0.55C14,0.55,14.24,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(84,168) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0-0.001h14v13.97v0.029l-3.431,0.001C4.731,13.999,0,7.731,0-0.001z" />
                  </g>
                </g>
                <g transform="translate(91,168) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(98,168) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(112,168) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(119,168) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(133,168) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,0v14c0,0-9.151-4.381-14-0.551C-0.001,13.449-0.242,0,13.999,0z" />
                  </g>
                </g>
                <g transform="translate(140,168) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(147,168) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(154,168) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(161,168) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(168,168) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(175,168) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0c0,0,9.15,4.38,14,0.55C14,0.55,14.24,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(189,168) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M-0.001,0l14,0c0,0-4.381,9.151-0.551,14C13.448,14-0.001,14.24-0.001,0z" />
                  </g>
                </g>
                <g transform="translate(63,175) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M-0.001,0l14,0c0,0-4.381,9.151-0.551,14C13.448,14-0.001,14.24-0.001,0z" />
                  </g>
                </g>
                <g transform="translate(105,175) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(112,175) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(119,175) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(126,175) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,13.999H0V0.028v-0.029h3.43C9.269-0.001,13.999,6.267,13.999,13.999z" />
                  </g>
                </g>
                <g transform="translate(140,175) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M-0.001,0l14,0c0,0-4.381,9.151-0.551,14C13.448,14-0.001,14.24-0.001,0z" />
                  </g>
                </g>
                <g transform="translate(161,175) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M-0.001,0l14,0c0,0-4.381,9.151-0.551,14C13.448,14-0.001,14.24-0.001,0z" />
                  </g>
                </g>
                <g transform="translate(196,175) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(70,182) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(84,182) scale(0.014,0.014)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M250,0c138.077,0,250,111.93,250,250c0,138.077-111.923,250-250,250C111.93,500,0,388.077,0,250C0,111.93,111.93,0,250,0z" />
                  </g>
                </g>
                <g transform="translate(105,182) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(126,182) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M-0.001,0l14,0c0,0-4.381,9.151-0.551,14C13.448,14-0.001,14.24-0.001,0z" />
                  </g>
                </g>
                <g transform="translate(175,182) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(182,182) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(189,182) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(196,182) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(70,189) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(77,189) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,13.999H0V0.028v-0.029h3.43C9.269-0.001,13.999,6.267,13.999,13.999z" />
                  </g>
                </g>
                <g transform="translate(91,189) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M14,14H0c0,0,4.38-9.15,0.55-14C0.55,0,14-0.24,14,14z" />
                  </g>
                </g>
                <g transform="translate(105,189) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0-0.001h14v13.97v0.029l-3.431,0.001C4.731,13.999,0,7.731,0-0.001z" />
                  </g>
                </g>
                <g transform="translate(112,189) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(119,189) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,13.999H0V0.028v-0.029h3.43C9.269-0.001,13.999,6.267,13.999,13.999z" />
                  </g>
                </g>
                <g transform="translate(140,189) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(147,189) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(154,189) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,13.999H0V0.028v-0.029h3.43C9.269-0.001,13.999,6.267,13.999,13.999z" />
                  </g>
                </g>
                <g transform="translate(168,189) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999-0.001v14H0.029H0v-3.431C0,4.729,6.267-0.001,13.999-0.001z" />
                  </g>
                </g>
                <g transform="translate(175,189) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(189,189) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(196,189) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(56,196) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M13.999,0v14c0,0-9.151-4.381-14-0.551C-0.001,13.449-0.242,0,13.999,0z" />
                  </g>
                </g>
                <g transform="translate(63,196) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(70,196) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(77,196) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(91,196) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M-0.001,0l14,0c0,0-4.381,9.151-0.551,14C13.448,14-0.001,14.24-0.001,0z" />
                  </g>
                </g>
                <g transform="translate(119,196) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M-0.001,0l14,0c0,0-4.381,9.151-0.551,14C13.448,14-0.001,14.24-0.001,0z" />
                  </g>
                </g>
                <g transform="translate(140,196) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0-0.001h14v13.97v0.029l-3.431,0.001C4.731,13.999,0,7.731,0-0.001z" />
                  </g>
                </g>
                <g transform="translate(147,196) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(154,196) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(161,196) scale(0.07,0.07)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <rect width="100" height="100" />
                  </g>
                </g>
                <g transform="translate(168,196) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M0,14V0h13.971H14v3.43C14,9.27,7.732,14,0,14z" />
                  </g>
                </g>
                <g transform="translate(189,196) scale(0.5,0.5)">
                  <g transform="" style={{fill: "rgb(255, 255, 255)"}}>
                    <path d="M-0.001,0l14,0c0,0-4.381,9.151-0.551,14C13.448,14-0.001,14.24-0.001,0z" />
                  </g>
                </g>
              </g>
            </mask>
          </defs>
        </g>
        <rect
          x="0"
          y="0"
          width="203"
          height="203"
          fill="url(#grad)"
          mask="url(#gmask)"
        />
        <g transform="translate(0,0) scale(0.49, 0.49)">
          <g transform="" style={{fill: "rgb(0, 0, 0)"}}>
            <path
              d="M21.475,99.992c-6.648,0-12.964-6.852-13.958-13.207L0.011-0.008l88.112,5.649c6.212,0.931,11.096,6.804,11.104,13.848
	l0.762,80.503H21.475z M85.706,85.71l-0.544-57.492c-0.005-5.029-3.495-9.233-7.931-9.898l-62.938-4.03l5.36,61.98
	c0.71,4.536,5.222,9.44,9.971,9.44H85.706z"
            />
          </g>
        </g>
        <g transform="translate(154,0) scale(0.49, 0.49)">
          <g
            transform=" translate(100, 0) scale(-1,1) "
            style={{fill: "rgb(0, 0, 0)"}}
          >
            <path
              d="M21.475,99.992c-6.648,0-12.964-6.852-13.958-13.207L0.011-0.008l88.112,5.649c6.212,0.931,11.096,6.804,11.104,13.848
	l0.762,80.503H21.475z M85.706,85.71l-0.544-57.492c-0.005-5.029-3.495-9.233-7.931-9.898l-62.938-4.03l5.36,61.98
	c0.71,4.536,5.222,9.44,9.971,9.44H85.706z"
            />
          </g>
        </g>
        <g transform="translate(0,154) scale(0.49, 0.49)">
          <g
            transform=" translate(0,100) scale(1,-1) "
            style={{fill: "rgb(0, 0, 0)"}}
          >
            <path
              d="M21.475,99.992c-6.648,0-12.964-6.852-13.958-13.207L0.011-0.008l88.112,5.649c6.212,0.931,11.096,6.804,11.104,13.848
	l0.762,80.503H21.475z M85.706,85.71l-0.544-57.492c-0.005-5.029-3.495-9.233-7.931-9.898l-62.938-4.03l5.36,61.98
	c0.71,4.536,5.222,9.44,9.971,9.44H85.706z"
            />
          </g>
        </g>
        <g transform="translate(14,14) scale(0.21, 0.21)">
          <g transform="" style={{fill: "rgb(69, 0, 71)"}}>
            <path
              d="M99,50c0-7.839-5.42-14.394-12.713-16.17C93.58,32.055,99,25.499,99,17.66C99,8.458,91.541,1,82.34,1
	c-7.838,0-14.395,5.421-16.17,12.713C64.395,6.421,57.838,1,50,1c-7.839,0-14.395,5.421-16.17,12.712C32.055,6.421,25.499,1,17.66,1
	C8.458,1,1,8.458,1,17.66c0,7.839,5.421,14.395,12.713,16.17C6.421,35.606,1,42.161,1,50c0,7.838,5.421,14.395,12.713,16.17
	C6.421,67.945,1,74.502,1,82.34C1,91.541,8.458,99,17.66,99c7.839,0,14.395-5.42,16.17-12.713C35.605,93.58,42.161,99,50,99
	c7.838,0,14.395-5.42,16.17-12.713C67.945,93.58,74.502,99,82.34,99C91.541,99,99,91.541,99,82.34c0-7.838-5.42-14.395-12.713-16.17
	C93.58,64.395,99,57.838,99,50z"
            />
          </g>
        </g>
        <g transform="translate(168,14) scale(0.21, 0.21)">
          <g transform="" style={{fill: "rgb(69, 0, 71)"}}>
            <path
              d="M99,50c0-7.839-5.42-14.394-12.713-16.17C93.58,32.055,99,25.499,99,17.66C99,8.458,91.541,1,82.34,1
	c-7.838,0-14.395,5.421-16.17,12.713C64.395,6.421,57.838,1,50,1c-7.839,0-14.395,5.421-16.17,12.712C32.055,6.421,25.499,1,17.66,1
	C8.458,1,1,8.458,1,17.66c0,7.839,5.421,14.395,12.713,16.17C6.421,35.606,1,42.161,1,50c0,7.838,5.421,14.395,12.713,16.17
	C6.421,67.945,1,74.502,1,82.34C1,91.541,8.458,99,17.66,99c7.839,0,14.395-5.42,16.17-12.713C35.605,93.58,42.161,99,50,99
	c7.838,0,14.395-5.42,16.17-12.713C67.945,93.58,74.502,99,82.34,99C91.541,99,99,91.541,99,82.34c0-7.838-5.42-14.395-12.713-16.17
	C93.58,64.395,99,57.838,99,50z"
            />
          </g>
        </g>
        <g transform="translate(14,168) scale(0.21, 0.21)">
          <g transform="" style={{fill: "rgb(69, 0, 71)"}}>
            <path
              d="M99,50c0-7.839-5.42-14.394-12.713-16.17C93.58,32.055,99,25.499,99,17.66C99,8.458,91.541,1,82.34,1
	c-7.838,0-14.395,5.421-16.17,12.713C64.395,6.421,57.838,1,50,1c-7.839,0-14.395,5.421-16.17,12.712C32.055,6.421,25.499,1,17.66,1
	C8.458,1,1,8.458,1,17.66c0,7.839,5.421,14.395,12.713,16.17C6.421,35.606,1,42.161,1,50c0,7.838,5.421,14.395,12.713,16.17
	C6.421,67.945,1,74.502,1,82.34C1,91.541,8.458,99,17.66,99c7.839,0,14.395-5.42,16.17-12.713C35.605,93.58,42.161,99,50,99
	c7.838,0,14.395-5.42,16.17-12.713C67.945,93.58,74.502,99,82.34,99C91.541,99,99,91.541,99,82.34c0-7.838-5.42-14.395-12.713-16.17
	C93.58,64.395,99,57.838,99,50z"
            />
          </g>
        </g>
      </g>
    </svg>
  );
};

export default QR;
