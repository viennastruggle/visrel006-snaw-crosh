import React, { useEffect } from "react";
import { motion } from "framer-motion";
import { useAvatarStore } from "../Context/AvatarStore";
import { useStoryStore } from "../Context/StoryStore";
const containerVariants = {
  hidden: {
    opacity: 0,
    x: "100vw",
  },
  visible: {
    opacity: 1,
    x: 0,
    transition: {
      type: "spring",
      mass: 0.4,
      damping: 8,
      when: "beforeChildren",
      staggerChildren: 0.4,
    },
  },
  exit: {
    x: "-100vw",
    transition: { ease: "easeInOut" },
  },
};

const childVariants = {
  hidden: {
    opacity: 0,
  },
  visible: {
    opacity: 1,
  },
};
const LobbyPage = ({ setShowModal }:any) => {
  const { avatar } = useAvatarStore();
  const { story } = useStoryStore();
  useEffect(() => {
    // setTimeout(() => {
    //   setShowModal(true);
    // }, 25000);
    console.log('Side effect for modal not used')
  }, [setShowModal]);

  return (
    <motion.div
      className="container order"
      variants={containerVariants}
      initial="hidden"
      animate="visible"
      exit="exit"
    >
      <h2>This is the journey of {avatar.name}.</h2>
      <motion.p variants={childVariants}>Here is a map, sucker!</motion.p>
      <motion.div variants={childVariants}>
        {story.map((chapter) => (
          <div key={chapter.id}>{chapter.id}</div>
        ))}
      </motion.div>
    </motion.div>
  );
};

export default LobbyPage;
