import React from "react";
// import { motion } from "framer-motion";
import Vimeo from "@u-wave/react-vimeo";

const containerVariants = {
  hidden: {
    opacity: 0,
    x: "100vw",
  },
  visible: {
    opacity: 1,
    x: 0,
    transition: {
      type: "spring",
      mass: 0.4,
      damping: 8,
      when: "beforeChildren",
      staggerChildren: 0.4,
    },
  },
  exit: {
    x: "-100vw",
    transition: { ease: "easeInOut" },
  },
};

// const childVariants = {
//   hidden: {
//     opacity: 0,
//   },
//   visible: {
//     opacity: 1,
//   },
// };
const LivePage = () => {
  return (
    <>
      {/* // <motion.div
    //   className="container order"
    //   variants={containerVariants}
    //   initial="hidden"
    //   animate="visible"
    //   exit="exit"
    // > */}
      <h2>Live Event</h2>
      <Vimeo width="100%" height="50vh" video="477657949" autopause={false} autoplay />
      <Vimeo width="100%" height="50vh" video="477660828" autopause={false} autoplay />
      {/* // </motion.div> */}
    </>
  );
};

export default LivePage;
