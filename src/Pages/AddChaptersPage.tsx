import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { motion } from "framer-motion";
import { Video } from "cloudinary-react";
import styled from "@emotion/styled";
import { chapters } from "../Presets/SnawCrosh";
import clsx from "clsx";
import { useStoryStore } from "../Context/StoryStore";
import { useAvatarStore } from "../Context/AvatarStore";
import useSound from "use-sound";
import { MimeType } from "../Models/Source.d";
import VideoUpload from "../Components/VideoUpload";
import { coverVariants } from "../Animations";

const containerVariants = {
  hidden: {
    opacity: 0,
    x: "100vw",
  },
  visible: {
    opacity: 1,
    x: 0,
    transition: {
      type: "spring",
      delay: 0.5,
    },
  },
  exit: {
    x: "-100vw",
    transition: { ease: "easeInOut" },
  },
};
const buttonVariants = {
  hover: {
    scale: 1.1,
    textShadow: "0px 0px 8px rgb(255,255,255)",
    boxShadow: "0px 0px 8px rgb(255,255,255)",
    transition: {
      duration: 0.3,
      yoyo: Infinity,
    },
  },
};

const Container = styled.div`
  ul {
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    grid-gap: 1rem;
    grid-template-rows: repeat (2, 1fr);
  }
  li {
    position: relative;
    margin: 0;
    span {
      /* display: flex; */
      position: absolute;
      top: 0.5rem;
      left: 1rem;
    }
  }
`;

const AddChaptersPage = () => {
  const { story, setStory, addChapter, removeChapter } = useStoryStore();
  const { avatar } = useAvatarStore();

  const [playActive] = useSound("/audio/pfff.mp3", { volume: 0.25 });
  const [playOn] = useSound("/audio/switch-off.mp3", { volume: 0.25 });

  const toggleChapter = (id: string) => {
    const found = story.find((item) => item.id === id) !== undefined;
    if (!found) addChapter({ id: id, progress: 0, order: 0 });
    else removeChapter(id);
  };

  useEffect(() => {
    setStory(avatar.storyPlot);
  }, [avatar.storyPlot, setStory]);

  return (
    <Container>
      <motion.div
        className="chapters container"
        variants={containerVariants}
        initial="hidden"
        animate="visible"
        exit="exit"
      >
        <h3>
          Step 2: Add Chapters
          <br />
          <Link to="/avatar">
            <motion.button variants={buttonVariants} whileHover="hover">
              Back
            </motion.button>
          </Link>
          &nbsp;
          <Link to="/lobby">
            <motion.button variants={buttonVariants} whileHover="hover">
              Start
            </motion.button>
          </Link>
        </h3>
      </motion.div>
      <motion.div
        className="chapters container"
        variants={coverVariants}
        initial="hidden"
        animate="visible"
        exit="exit"
      >
        <ul>
          {chapters.map((chapter) => {
            return (
              <motion.li
              drag
        dragConstraints={{ left: 0, top: 0, right: 0, bottom: 0 }}
        dragElastic={1}
                whileHover={{ scale: 1.3, color: "#f8e112" }}
                transition={{ type: "spring", stiffness: 300 }}
                key={chapter.id}
                onClick={() => toggleChapter(chapter.id)}
                onMouseOver={() => playActive()}
                onMouseUp={() => {
                  playOn();
                }}
              >
                {chapter.video?.source.mimeType === MimeType.MOV &&
                chapter.video?.source.publicId ? (
                  <>
                    <Video
                      publicId={chapter.video?.source.publicId}
                      width={300}
                      controls
                    />
                    {/* <Image publicId={chapter.id} crop="scale" /> */}
                  </>
                ) : (
                  <div style={{ height: "20vh", marginBottom: "1rem" }}>
                    <VideoUpload
                      text={`Upload`}
                      id={"yt"}
                      folder={"visrel006-snawcrosh/avatars"}
                    />
                  </div>
                )}
                <span
                  className={clsx({
                    active: story.find((c) => c.id === chapter.id),
                  })}
                >
                  {chapter.title}
                </span>
              </motion.li>
            );
          })}
        </ul>
      </motion.div>
    </Container>
  );
};

export default AddChaptersPage;
