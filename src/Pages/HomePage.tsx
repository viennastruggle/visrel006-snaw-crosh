import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { motion } from "framer-motion";
import styled from "@emotion/styled";
import Cover from "../Components/Cover";
import useSound from "use-sound";
import { meta } from "../Presets/SnawCrosh";
import ReactMarkdown from "react-markdown";
import {
  buttonVariants,
  containerVariants,
  coverVariants,
} from "../Animations";

const Container = styled.div`
  .description {
    padding: 1rem;
  }
`;

const HomePage = () => {
  const [playActive] = useSound("/audio/pfff.mp3", { volume: 0.25 });
  const [playOn] = useSound("/audio/switch-off.mp3", { volume: 0.25 });

  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 5000);
  }, []);

  return (
    <Container>
      <section className="split-section">
        <motion.div
          className="home cover"
          variants={coverVariants}
          initial="hidden"
          animate="visible"
          exit="exit"
        >
          <Cover />
        </motion.div>
        <motion.div
          className="home"
          variants={containerVariants}
          initial="hidden"
          animate="visible"
          exit="exit"
        >
          <motion.div
            className="home container"
            // initial={{x:'100vw'}}
            // animate={{x:0}}
            variants={containerVariants}
            initial="hidden"
            animate="visible"
            exit="exit"
            // transition={{type:'spring',delay:0.5}}
          >
            <h3>
              Introduction
              <br />
              {!isLoading && (
                <>
                {/* // <motion.div
                //   initial={{ y: 250 }}
                //   animate={{ y: 0 }}
                //   transition={{ delay: 2, type: "spring", stiffness: 120 }}
                // > */}
                <motion.button
                  variants={buttonVariants}
                  whileHover="hover"
                  onMouseOver={() => playActive()}
                  onMouseUp={() => {
                    playOn();
                  }}
                >
                  <Link to="/avatar">Setup your trip, baby!</Link>
                </motion.button>
                <motion.button
                  variants={buttonVariants}
                  whileHover="hover"
                  onMouseOver={() => playActive()}
                  onMouseUp={() => {
                    playOn();
                  }}
                >
                  <Link to="/live">Live</Link>
                </motion.button>
                </>
              )}
            </h3>
          </motion.div>
          {/* <h1>{meta.title}</h1> */}
          <ReactMarkdown className="description" source={meta.description} />
        </motion.div>
      </section>
    </Container>
  );
};

export default HomePage;
