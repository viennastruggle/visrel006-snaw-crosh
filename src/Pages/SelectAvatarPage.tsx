import React from "react";
import { Link } from "react-router-dom";
import { motion } from "framer-motion";
import { avatars } from "../Presets/SnawCrosh";
import { useAvatarStore } from "../Context/AvatarStore";
import clsx from "clsx";
import { Avatar } from "../Models/Avatar";

import useSound from "use-sound";

// import {
//   CarouselProvider,
//   Slider,
//   Slide,
//   ButtonBack,
//   ButtonNext,
// } from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";

import AvatarDetail from "../Components/AvatarDetail";
import {
  buttonVariants,
  containerVariants,
  coverVariants,
} from "../Animations";
import styled from "@emotion/styled";

const Container = styled.div`
.split-section {
  padding: 1rem;
}
`;

const SelectAvatarPage = () => {
  const { avatar, setAvatar } = useAvatarStore();

  const [playActive] = useSound("/audio/pfff.mp3", { volume: 0.25 });
  const [playOn] = useSound("/audio/switch-off.mp3", { volume: 0.25 });

  return (
    <Container>
      <motion.div
        className="base container"
        // initial={{x:'100vw'}}
        // animate={{x:0}}
        variants={containerVariants}
        initial="hidden"
        animate="visible"
        exit="exit"
        // transition={{type:'spring',delay:0.5}}
      >
        <h3>
          Step 1: Choose Your Avatar
          {avatar.id !== "" && (
            <motion.div className="next" variants={containerVariants}>
                        <Link to="/">
            <motion.button variants={buttonVariants} whileHover="hover">
              Back
            </motion.button>
          </Link>
          &nbsp;
              <Link to="/chapters">
                <motion.button
                  variants={buttonVariants}
                  whileHover="hover"
                  onMouseOver={() => playActive()}
                  onMouseUp={() => {
                    playOn();
                  }}
                >
                  Next{" "}
                </motion.button>
              </Link>
            </motion.div>
          )}
        </h3>
      </motion.div>
      <section className="split-section">
        <motion.div className="left" variants={coverVariants}>
          <ul>
            {avatars.map((a: Avatar) => {
              return (
                <motion.li
                  className="next"
                  drag
                  dragConstraints={{ left: 0, top: 0, right: 0, bottom: 0 }}
                  dragElastic={1}
                  whileHover={{ scale: 1.3, color: "#f8e112", originX: 0 }}
                  transition={{ type: "spring", stiffness: 300 }}
                  key={a.id}
                  onClick={() => setAvatar(a)}
                  onMouseOver={() => playActive()}
                  onMouseUp={() => {
                    playOn();
                  }}
                >
                  <span className={clsx({ active: avatar === a })}>
                    {a.name}
                  </span>
                </motion.li>
              );
            })}
          </ul>
        </motion.div>
        <AvatarDetail avatar={avatar} />
      </section>
    </Container>
  );
};
export default SelectAvatarPage;
