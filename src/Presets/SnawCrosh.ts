import { AssetType } from "../Models/Asset.d";
import { Avatar } from "../Models/Avatar";
import { Chapter } from "../Models/Chapter";
import { MimeType } from "../Models/Source.d";

type StoryMeta = {
  id: string; // base folder for story
  title: string;
  description: string;
};

export const meta: StoryMeta = {
  title: "SNAW CRO$H - A virtual sounddrug",
  description: `Welcome to the year 2019 AD,
  
  where a band of digital outcasts tries to find their place in
  a society ruled by coperations, mafia and vigilante entrepreneurs. 
  If that wasn't already challange enough, they are forced to take action to prevent 
  the outbreak of pandemic cause by a virus disguised as a drug - SNAW CRO$H.
  Thanks to their education the musicians and characters in this cyberpunk reality
  merge the post Covid-19 world with the fictional meta vers of Neal Stephenson, 
  and the futurist melodies of Gustav Mahler.

  "Nicht mehr von dieser Welt. Ausgezeichnet."
  *FAZ*


  "Like nothing ever seen. This future masterpiece represents digital Zeitgeist
  and blends all genres seamlessly. The sound of Cuban German utopia. Fabolous, 
  Bravo, Stand up for the god damn ovations."
  *The Economist*

  
  "Sick! I transcendet."
  *Hedy Lamarr*
  `,
  id: "visrel006-snawcrosh",
};

export const chapters: Chapter[] = [
  {
    id: "snawcrosh-policei",
    title: "Policei",
    audio: {
      source: {
        publicId:
          "visrel006-snawcrosh/syn3a-sc-08-policei-mix-2018-04-05_dwx42q",
        url: "/audio/syn3a-sc-08-policei-mix-2018-04-05.mp3",
        mimeType: MimeType.MP3,
      },
      rate: 1,
      volume: 1,
      position: 0,
      isPlaying: false,
    },
  },
  {
    id: "snawcrosh-librarian",
    title: "Librarian",
    audio: {
      source: {
        publicId:
          "visrel006-snawcrosh/syn3a-sc-09-librarian-mix-2018-04-06_jfg6i0",
        url: "/audio/syn3a-sc-09-librarian-mix-2018-04-06.mp3",
        mimeType: MimeType.MP3,
      },
    },
  },
  {
    id: "snawcrosh-pad",
    title: "Pad",
    audio: {
      source: {
        publicId:
          "visrel006-snawcrosh/syn3a-sc-10-pad-mix-24bit-2018-02-28_qjs0do",
        url: "/audio/syn3a-sc-10-pad-mix-24bit-2018-02-28.mp3",
        mimeType: MimeType.MP3,
      },
    },
  },
  {
    id: "snawcrosh-chase",
    title: "Chase",
    audio: {
      source: {
        publicId:
          "visrel006-snawcrosh/syn3a-sc-03-chase-mix-24bit-2018-04-06_7dfhjk",
        url: "/audio/syn3a-sc-03-chase-mix-24bit-2018-04-06",
        mimeType: MimeType.MP3,
      },
    },
  },
  {
    id: "snawcrosh-first-look",
    title: "First Look",
    audio: {
      source: {
        publicId:
          "visrel006-snawcrosh/syn3a-sc-07-first-look-mix-24bit-2018-04-06_opsnjo",
        url: "/audio/syn3a-sc-07-first-look-mix-24bit-2018-04-06.mp3",
        mimeType: MimeType.MP3,
      },
    },
  },
  {
    id: "snawcrosh-black-sun",
    title: "Black Sun",
    audio: {
      source: {
        publicId:
          "visrel006-snawcrosh/syn3a-sc-06-black-sun-mix-24bit-2018-04-04_mlrhno",
        url: "/audio/syn3a-sc-06-black-sun-mix-24bit-2018-04-04.mp3",
        mimeType: MimeType.MP3,
      },
    },
  },
  {
    id: "snawcrosh-alaska",
    title: "Alaska",
    audio: {
      source: {
        publicId:
          "visrel006-snawcrosh/syn3a-sc-02-alaska-mix-24bit-2018-04-05_q2skby",
        url: "/audio/syn3a-sc-02-alaska-mix-24bit-2018-04-05.mp3",
        mimeType: MimeType.MP3,
      },
    },
  },
  {
    id: "snawcrosh-rathing",
    title: "Rathing",
    audio: {
      source: {
        publicId:
          "visrel006-snawcrosh/syn3a-sc-05-rat-thing-mix-24bit-2018-04-07_ev4zid",
        url: "/audio/syn3a-sc-05-rat-thing-mix-24bit-2018-04-07.mp3",
        mimeType: MimeType.MP3,
      },
    },
  },
  {
    id: "snawcrosh-metavers-3",
    title: "Metavers 3",
    audio: {
      source: {
        publicId:
          "visrel006-snawcrosh/syn3a-sc-04-metavers-3-mix-24bit-2018-04-05_acqn26",
        url: "/audio/syn3a-sc-04-metavers-3-mix-24bit-2018-04-05.mp3",
        mimeType: MimeType.MP3,
      },
    },
  },
  {
    id: "snawcrosh-this-is-america",
    title: "This is America",
    audio: {
      source: {
        publicId:
          "visrel006-snawcrosh/syn3a-sc-01-this-is-america-mix-2018-04-04_uoinrj",
        url: "/audio/syn3a-sc-01-this-is-america-mix-2018-04-04.mp3",
        mimeType: MimeType.MP3,
      },
    },
  },
];

export const avatars: Avatar[] = [
  {
    id: "yt",
    name: "Y.T.",
    model: {
      source: {
        publicId: "visrel006-snawcrosh/avatars/demo-avatar-video",
        url: "/video/demo-avatar-video.mov",
        mimeType: MimeType.MOV,
      },
    },
    storyPlot: [
      {
        id: "snawcrosh-policei",
        progress: 0,
        order: 1,
      },
      // {
      //   id: "snawcrosh-librarian",
      //   progress: 0,
      //   order: 2,
      // },
      {
        id: "snawcrosh-pad",
        progress: 0,
        order: 3,
      },
      {
        id: "snawcrosh-chase",
        progress: 0,
        order: 4,
      },
      {
        id: "snawcrosh-black-sun",
        progress: 0,
        order: 5,
      },
      {
        id: "snawcrosh-alaska",
        progress: 0,
        order: 6,
      },
      {
        id: "snawcrosh-first-look",
        progress: 0,
        order: 7,
      },
      {
        id: "snawcrosh-metavers-3",
        progress: 0,
        order: 8,
      },
      {
        id: "snawcrosh-rathing",
        progress: 0,
        order: 9,
      },
      {
        id: "snawcrosh-this-is-america",
        progress: 0,
        order: 10,
      },
    ],
    theme: {
      backgroundColor: "white",
      headerImage: {
        mimeType: MimeType.PNG,
        url: "path/to/header",
      },
    },
    description: `I am 15 year old delivery girl who enjoys her female hood.
        Danger and adventure excite me, yet I love my mother and would 
        not dare to get her into harms way. I work as a part time deliverator. 
        Don't mess with me, my hoverboard and my magentic lasso. 
        And to all the pervs out there, no chance. I wear a sedative-equiped chastity belt, 
        just in case no really doesn't mean no to you.`,
    assets: [
      {
        id: "item-001",
        type: AssetType.VIRTUAL,
        name: "Sedative equiped chastity belt",
        manifest: {
          url: "path/to/manifest",
          isUnique: true,
          tradeable: false,
        },
        description: `This wearable will inject a tiny portion of sedative into the 
          penis in case somebody tries to enter a vagina.`,
        source: {
          url: "path/to/",
          mimeType: MimeType.PNG,
        },
      },
      {
        id:
          "kitty-token-ce85b99cc46752fffee35cab9a7b0278abb4c2d2055cff685af4912c49490f8d",
        type: AssetType.TOKEN,
        name: "Cryptonik Kitten",
        manifest: {
          url: "path/to/manifest",
          isUnique: true,
          tradeable: false,
        },
        description: `Rescued in the far north of Alaska. This kitten is issued by the Bank of Aleut.`,
        source: {
          publicId: "visrel006-snawcrosh/5a521ad42f93c7a8d5137fa5_c909id",
          url: "path/to/",
          mimeType: MimeType.PNG,
        },
      },
    ],
  },
  {
    id: "raven",
    name: "Raven a.k.a. Dmitri Ravinoff",
    model: {
      source: {
        publicId: "visrel006-snawcrosh/raven",
        mimeType: MimeType.MOV,
      },
    },
    storyPlot: [],
    description: `When Raven meets Y.T. and introduces himself as an Aleut, 
    she hasn't heard of that ethnic group, and Raven tells her in no 
    uncertain terms why this is: "'That's because we've been f***ed over […] 
    worse than any other people in history'" (47.15). With that kind of 
    perspective, it sort of makes sense that Raven has this huge grudge against, 
    well, practically everyone. He feels oh so very wronged by the world.`,
  },
  {
    id: "hiro",
    name: "Hiro Protagonist",
    storyPlot: [],
    description: `
    The Deliverator

Hiro is nothing if not hardcore. His stint as a pizza deliverer, nay, 
The Deliverator, illustrates this. Clad in black armorgel and driving a 
car with "enough potential energy to fire a pound of bacon into the Asteroid 
Belt" (1.5), the Deliverator is at the top of his game, and he has never 
delivered a late pizza. Well, until the doomed delivery incident.`,
  },
  {
    id: "librarian",
    name: "Librarian",
    storyPlot: [],
    description: `This computer program (or daemon) has been created to resemble 
    and act like a person in order to facilitate research. It "looks like a pleasant, 
    fifty-ish, silver-haired, bearded man with bright blue eyes, wearing a V-neck 
    sweater over a work shirt, with a coarsely-woven, tweedy-looking wool tie. 
    The tie is loosened, the sleeves pushed up" (13.36). In other words, your 
    typical nerdy librarian/professor type.`,
  },
  { id: "mandymozart", name: "Mandy Mozart", storyPlot: [] },
  {
    id: "otto",
    name: "Otto Oscar Hernandez",
    storyPlot: [],
    description: `This tall 2 meter Cuban architect is not your regular pencil pusher. 
    His athletic statue tell you right away, he has a past in track and field - hammer throwing, 
    they called him Thor of Havanna under the Castro regime. He is banned from Uncle Enzo's teretories
    and lives in exil in Germany dedicating himself to performance and designing virutal analog realities.
    Surely, his rendering of Leni Riefenstahlesque light sculptures must
    have gone to your head as well when you were reading the novel. His love for romantic music
    is the reason this soundtrack is renting ideas and melodies from Gustav Mahler - A futurist from 
    another mother.`,
  },
  {
    id: "ryu-una",
    name: "Ryu Una",
    storyPlot: [],
    description: `Splatter, gore and dark techno. Once lover of Sushi K she made her voyage to 
    Berlin moulding the darkest avatars in the meta vers.`,
  },
  {
    id: "shin-hyo-jin",
    name: "Shin Hyo-Jin",
    storyPlot: [],
  },
  {
    id: "juanita-marquez",
    name: "Juanita Marquez",
    storyPlot: [],
    description: `Mother of the Metaverse
    Because she designed the way faces work in the 
    Metaverse, Juanita can be considered one of its main 
    inventors. So it's kind of odd that she doesn't 
    set foot in it very often anymore, eh?`,
  },
  {
    id: "l-bob-rife",
    name: "L. Bob Rife",
    storyPlot: [],
    description: `This communications monopolist is something of a 
    mystery at first: He's super rich, puts a lot of money into 
    religious organizations, and is interesting to Juanita, 
    which makes him interesting to Hiro. After some research, 
    Hiro discovers that Rife is at the center of a global conspiracy: 
    he's manufacturing Snow Crash (the physical drug) to control the 
    bulk of the population, the unwashed masses. And he's also manufacturing 
    Snow Crash (the computer virus in binary code) in an effort to control 
    the information elite, the computer programmers.`,
  },
  {
    id: "uncle-enzo",
    name: "Uncle Enzo",
    storyPlot: [],
    description: `Are you on any billboards? Yeah, we didn't think so. But 
    then again you also probably don't run the Mafia, and as such, don't feel 
    the need to launch a PR campaign to convince the masses the family you're 
    in charge of is a-okay. This, however, is exactly what Uncle Enzo does.`,
  },
  {
    id: "da5id-meier",
    name: "Da5id Meier",
    storyPlot: [],
    description: `Hiro's buddy, and the owner of hacker metropolis The Black Sun, 
    Da5id is a guy who knows where he stands in life. First, there's his heritage: 
    "His folks were Russian Jews from Brooklyn and had lived in the same brownstone 
    for seventy years after coming from a village in Latvia where they had 
    lived for five hundred years" (7.33). Sounds like a lot of stability and 
    tradition to back him up.

    Da5id is also "an only child who had always been first in his class in 
    everything, and […] got his master's degree in computer science from Stanford" 
    (7.33). In other words, a real idiot, this one. He married Juanita, 
    who worked for him for a while, but their relationship ended in divorce, 
    and after Da5id started The Black Sun and got rich, he moved into "a modernist 
    castle with a high turret on one end" (24.69) located near the Hollywood 
    sign in Los Angeles. Da5id and Hiro and their hacker buddies used to party there.
    
    After Da5id had to let Hiro go from the company, he's looked out for Hiro, 
    feeding him tidbits of useful knowledge and letting him use company resources.
    This makes it all the more a shame when he assumes his computer's defenses 
    will keep his brain safe from Snow Crash, and suffers a stroke. Sadly, it 
    doesn't seem likely that he'll recover.`,
  },
  {
    id: "buenoventura",
    name: "Buenoventura",
    storyPlot: [],
  },
  {
    id: "neal-stephenson",
    name: "Neal Stephenson",
    storyPlot: [],
    description: `Author of the novel this soundtrack is based on.`,
  },
  {
    id: "monsterfrau",
    name: "Monsterfrau",
    storyPlot: [],
  },
  {
    id: "bartellow",
    name: "Bartellow",
    storyPlot: [],
  },
  {
    id: "gustav-mahler",
    name: "Gustav Mahler",
    storyPlot: [],
  },
];

export default {
  chapters,
  avatars,
};
