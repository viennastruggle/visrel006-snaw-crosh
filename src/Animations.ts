import { Variants } from "framer-motion";

export const coverVariants:Variants = {
  hidden: {
    x: "-30vw",
    opacity: 0,
  },
  visible: {
    opacity: 1,
    x: "0",
    transition: { delay: 1, type: "spring", stiffness: 120, duration: 4 },
  },
  exit: {
    opacity: 0,
    x: "+100vw",
    transition: { ease: "easeInOut" },
  },
};

export const containerVariants = {
    hidden: {
      x: "+30vw",
      opacity: 0,
    },
    visible: {
      opacity: 1,
      x: "0",
      transition: { delay: 1.2, type: "spring", stiffness: 120, duration: 5 },
    },
    exit: {
      x: "-100vw",
      transition: { ease: "easeInOut" },
    },
  };

  export const buttonVariants = {
    hover: {
      scale: 1.1,
      textShadow: "0px 0px 8px rgb(255,255,255)",
      boxShadow: "0px 0px 8px rgb(255,255,255)",
      transition: {
        duration: 0.3,
        yoyo: Infinity,
      },
    },
  };