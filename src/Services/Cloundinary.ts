import React, { useEffect, useState } from "react";
import axios, { AxiosResponse } from "axios";

import {
  CLOUDINARY_API_KEY,
  CLOUDINARY_CLOUD_NAME,
  CLOUDINARY_SIGNATURE_ENDPOINT,
} from "../config";
import { Configuration } from "cloudinary-core";

export enum CloudinarySourceEnum {
  LOCAL = "local",
  CAMERA = "camera",
}

// The error coming from the generated axios client
type ErrorType = {
  response: AxiosResponse;
  message: string;
  status: number;
};

declare global {
  interface Window {
    cloudinary: {
      openUploadWidget: (
        options: {
          cloudName: string;
          uploadPreset: string;
          apiKey?: string;
          uploadSignature?: any;
          publicId?: string;
          folder?: string;
          resourceType?: "video" | "image";
          sources?: CloudinarySourceEnum[];
        },
        callback: (error: any, result: any) => void
      ) => void;
      createUploadWidget: (
        options: any,
        callback: (error: any, result: any) => void
      ) => void;
    };
  }
}

const useCloundinary = ({
  folder,
  preset,
}: {
  folder: string;
  preset: string;
}) => {

  const [ result, setResult ] = useState({});
  const [ loading, setLoading ] = useState(false);
  const [ hasError, setHasError] = useState(false);

  useEffect(() => {
    const handleError = (error: ErrorType): Promise<ErrorType> => {
      switch (error.status) {
        case 401:
          console.error("401");
          break;
      }
      return Promise.reject({
        type: "ServerError",
        message: "Missing error response body",
        status: error.status,
      });
    };

    axios.interceptors.response.use(
      (response) => response,
      (error: ErrorType) => handleError(error)
    );

    try {
      init()
    }
    catch {

    }

  }, []); // subscribe to changes if needed

  const generateSignature = (callback: Function, params_to_sign: any):void => {
    console.log("generateSignature", callback, params_to_sign);

    axios
      .get(CLOUDINARY_SIGNATURE_ENDPOINT, { data: params_to_sign })
      .then((signature: any) => {
        callback(signature);
      })
      .catch((error: any) => {
        console.log(error);
      })
      .then(function () {
        console.log("complete");
      });
  };

  const createWidget = ():void => {
    setLoading(true);
    window.cloudinary.createUploadWidget(
      {
        cloudName: CLOUDINARY_CLOUD_NAME,
        uploadPreset: preset,
        apiKey: CLOUDINARY_API_KEY,
        uploadSignature: generateSignature,
      },
      (error: any, result: any) => {
        console.log(error, result);
        setResult(result);
        setLoading(false);
        setHasError(true);
      }
    );
  };

  const init = function () {
      window.cloudinary.openUploadWidget(
        {
          cloudName: CLOUDINARY_CLOUD_NAME,
          uploadPreset: "ml_default",
          apiKey: CLOUDINARY_API_KEY,
          uploadSignature: generateSignature,
        },
        (error: any, result: any) => {
          if (result.event === "success") {
            console.log("result", result);
            setResult(result)
          }
          if (error) console.error(error);
          setHasError(true);
        }
      );
  };

  return {
    result,
    loading,
    hasError
  };
};

export default useCloundinary;
