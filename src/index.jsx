import { CloudinaryContext } from "cloudinary-react";
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { CLOUDINARY_CLOUD_NAME } from "./config";
import App from "./App";
import "./index.css";
import * as serviceWorker from "./serviceWorker";


ReactDOM.render(
  <React.StrictMode>
    <CloudinaryContext cloudName={CLOUDINARY_CLOUD_NAME}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </CloudinaryContext>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
